#Sơ ri SQL Index Fragmentation(Phân mảnh) #
Sai lầm thường gặp:
- Số lượng bản ghi ít thì câu lệnh sẽ nhanh
- Cứ quét Full Table là sẽ tệ hơn sử dụng Index
- Cứ đầu tư server khủng là tăng mạnh hiệu năng
## Kịch bản select vẫn làm việc cực lâu với table 0 row (nôm na là tác hại phân mảnh)##
Ví dụ cho 2 table(Fast và Slow) cùng cấu trúc, có cùng số lượng bản ghi(0 row), đều cùng thực thi câu lệnh **Select count(*)** và chung chiến lược **FULL SCAN TABLE** nhưng table Fast lại thực thi rất nhanh trong khi table Slow lại chậm hơn rất nhiều

Lí do: Số lượng **block** dữ liệu phải thực hiện trên table Slow nhiều hơn rất nhiều so với table Fast và nhiều **block** của table Slow nằm trong đĩa cứng gây tốn tài nguyên như bộ nhớ cache, CPU, I/O,....

Giải thích: Chúng ta có thể hình dung table giống như cốc nước, hành động thêm dữ liệu vào table là ta đổ nước vào cốc

Bây giờ khi ta muốn đổ nước ra ngoài thì chuyện gì sẽ xảy ra với mực nước đã được **đánh dấu ở phía trên**

Tuy lượng nước trong cốc đã giảm nhưng **vạch đánh dấu** vẫn là ở phía trên, vấn đề này đã phản ánh khi bạn **delete** dữ liệu trong table.

Số lượng bản ghi giảm đi nhưng **vạch đánh dấu dữ liệu** không hề giảm và khi CSDL tìm kiếm thông tin nó vẫn sẽ chạy đến tận vạch kia để kiểm tra => dù số lượng bản ghi có = 0 mà **vạch đánh dấu dữ liệu(high water mark)** cao thì thời gian chạy vẫn sẽ lâu
![](https://lh3.googleusercontent.com/o-0LkDIVuF-ZcJoPtYCiraOLesEspZKcPfP72A0-2KWELmecq0w0D9CxmHcTygxIznXte2FPB1f1lB62FxUGwPzEsSXVJBV8I_GD3QEgtndHdLlyP24JgKqduYv-GFQtaJpY5f6B)
## Việc hiểu về High Water Mark sẽ giúp chúng ta tối ưu trong thực tế ra sao ##
Do trong quá trình insert dữ liệu, CSDL sẽ thực hiện lấp đầy những vùng trống bên dưới High Water Mark(việc này sẽ làm giảm hiệu năng), trong những hệ thống yêu cầu insert dữ liệu nhanh chóng thì chúng ta bắt buộc phải insert phía trên High Water Mark để tăng tốc độ insert. Với những bảng bị phân mảnh quá nhiều thì chúng ta phải tối ưu lại để giảm các blocks khi select
## Xem xong kịch bản rồi thì định nghĩa thôi ##
Với những bảng có sử dụng Index mà những thao tác thay đổi dữ liệu xảy ra nhiều thì những Index trên bảng đó sẽ bị phân mảnh. Sự phân mảnh của Index được chia làm 2 loại:
- Internal Fragmentation: Khi trang nhớ (Page) lưu Index có nhiều khoảng trống, dẫn đến việc SQL Server sẽ phải mất thêm nhiều chi phí (cần phải đọc nhiều trang hơn) khi quét qua toàn bộ Index.
- External Fragmentation: Là hiện tượng khi thứ tự logic của các trang nhớ không khớp với thứ tự vật lý của các trang nhớ, và nó cũng tạo ra các khoảng trống trong trang nhớ
## Hiểu rõ phân mảnh rồi thì làm thế nào để phát hiện phân mảnh? ##
Kiểm tra độ phân mảnh có 2 cách:
- Thực thi câu lệnh SQL: 
```sql
SELECT * FROM sys.dm_db_index_physical_stats(DB_ID(N'TEN_DATABASE'), OBJECT_ID(N'TEN_BANG'), NULL, NULL , 'DETAILED');
GO
```
hoặc
```sql
SELECT S.name as 'Schema',
T.name as 'Table',
I.name as 'Index',
DDIPS.avg_fragmentation_in_percent,
DDIPS.page_count
FROM sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL, NULL, NULL) AS DDIPS
INNER JOIN sys.tables T on T.object_id = DDIPS.object_id
INNER JOIN sys.schemas S on T.schema_id = S.schema_id
INNER JOIN sys.indexes I ON I.object_id = DDIPS.object_id
AND DDIPS.index_id = I.index_id
WHERE DDIPS.database_id = DB_ID()
and I.name is not null
AND DDIPS.avg_fragmentation_in_percent > 0
ORDER BY DDIPS.avg_fragmentation_in_percent desc
```
- Tìm mục Indexes xong chuột phải index chọn properties xong chuyển sang tab Fragmentation
## Giải quyết phân mảnh thế nào ##
**Nếu độ phân mảnh trong khoảng 15 - 30% thì áp dụng reorganize (nên thường xuyên sử dụng)**
- Sử dụng Query:
```sql
USE TEN_DATABASE;
GO
ALTER INDEX [TEN_INDEX|ALL] ON TEN_BANG REORGANIZE;
GO
```
- Sử dụng menu tùy chọn: Indexes >> Click phải chọn Reorganize All hoặc chọn Tên Index rồi click phải chọn Reorganize

**Trong trường hợp độ phân mảnh > 30% thì áp dụng rebuild**
- Sử dụng Query:
```sql
USE TEN_DATABASE;
GO
ALTER INDEX [TEN_INDEX|ALL] ON TEN_BANG
REBUILD WITH (FILLFACTOR = [0-100], SORT_IN_TEMPDB = ON,
              STATISTICS_NORECOMPUTE = ON, ONLINE = ON|OFF);
GO
```
Nếu người dùng thực hiện **REBUILD** với **ONLINE = OFF**, thì tài nguyên đối tượng (Table) của INDEX sẽ không thể truy cập được cho đến khi kết thúc quá trình REBUILD hoàn thành. Nó cũng ảnh hưởng đến nhiều giao dịch khác, được liên kết với đối tượng này. Xây dựng lại hoạt động chỉ mục sẽ tạo lại chỉ mục. Do đó, nó tạo ra số liệu thống kê mới và thêm các bản ghi nhật ký của INDEX trong tệp nhật ký giao dịch CSDL.

**FILLFACTOR:** Khi một index được tạo ra hoặc tổ chức lại, giá trị fill-factor sẽ xác định phần trăm dung lượng ở mỗi nốt lá sẽ chứa dữ liệu, phần còn lại là không gian trống. Ví dụ, fill-factor là 80 nghĩa là 20% nốt lá sẽ trống, dùng để mở rộng chỉ mục khi dữ liệu được thêm vào bảng.
|Tính chất|Giá trị FILLFACTOR|Giải thích|
|:-------:|:----------------:|:--------:|
|Mặc định|100|Không tốt lắm, nên thay đổi tuỳ vào tính chất bảng|
|Cho phép trang nhớ lưu tối đa|0||
|Bảng tĩnh|0 hoặc 100||
|Bảng ít Update|95||
|Bảng hay thay đổi|70 đến 90|Giá trị cho những bảng này cần tự kiểm tra và lựa chọn giá trị thích hợp với từng hệ thống|
|Bảng có Clustered Index trên cột Identity hoặc dựa trên cột Identity|0 hoặc 100|Những bảng có tính chất như vậy không bao giờ xuất hiện External Fragmentation do giá trị mới luôn nhận giá trị lớn nhất, và sẽ được thêm vào vị trí cuối cùng trong bảng|

**PAD_INDEX:** Giá trị mặc định của pad_index là OFF. Khi bật lên ON, phần trăm không gian trống xác định bởi fill-factor sẽ được áp dụng cho các nốt trung gian của chỉ mục. Lưu ý, pad_index chỉ có tác dụng khi có khai báo fill-factor

**SORT_IN_TEMPDB:** Mặc định là OFF. KHi bật lên ON, sẽ làm giảm thời gian cần thiết để tạo index nếu tempdb nằm trên một ổ đĩa khác với CSDL người dùng. Tuy nhiên, điều này sẽ làm nặng ổ đĩa được sử dụng trong quá trình xây dựng index
- Sử dụng menu tùy chọn (tương tự cách Reorganize).
