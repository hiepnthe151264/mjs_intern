## Message Queue - Cách mà microservice giao tiếp với nhau ##

## RabbitMQ ##
### Tổng quát ###
RabbitMQ là một message broker hay còn gọi là phần mềm quản lý hàng đợi message. Nó sử dụng giao thức AMQP – Advanced Message Queue Protocol, một giao thức nâng cao phổ biến có thể gửi dữ liệu (message) thông qua cơ chế hàng đợi. Thực tế ban đầu chỉ là AMQP nhưng sau đó đã được phát triển để hỗ trợ Streaming Text Oriented Messaging Protocol (STOMP), Message Queuing Telemetry Transport (MQTT), và những giao thức khác. Nói đơn giản, đây là phần mềm định nghĩa hàng đợi một ứng dụng khác có thể kết nối đến để bỏ message vào và gửi message dựa trên nó, đóng vai trò trung gian lưu trữ cũng như điều phối các yêu cầu(message) giữa người gửi(producer) và người nhận(consumer).
#### Message Broker là gì? ####
Message broker là một chương trình trung gian được thiết kế để Validating, transforming, routing messages. Chúng phục vụ nhu cầu giao tiếp giữa các ứng dụng với nhau.

Với Message broker, ứng dụng nguồn (producer) gửi một message đến một server process mà nó có thể cung cấp việc sắp xếp dữ liệu, routing (Định tuyến), message translation, persistence và delivery tất cả các điểm đến thích hợp (consumer).

Có 2 hình thức giao tiếp cơ bản với Message broker là:
- Publisher và Subcriber(Topics)
- Point to point(Queue)
### Khi nào và tại sao lại dùng RabbitMQ ###
#### Vấn đề ####
Đối với các hệ thống sử dụng kiến trúc microservice thì việc gọi giữa các serivce khá nhiều, khiến cho luồng xử lý phức tạp. Khi mức độ trao đổi giữa các service tăng lên khiến cho việc lập trình trở nên khó khăn hơn (maintain khó).
Khi phát triển ứng dụng phải làm sao cho lập trình viên có thể tập trung logic vào các lớp BL, domain thay vì các công việc trao đổi ở tầng infracstructure.
Với các hệ thống phân tán, khi việc trao đổi với thành phần đòi hỏi chúng phải biết nhau => đâm ra khó code mà nếu 1 thành phần phải biết nhiều thành phần khác => khó maintain, debug.
#### Lợi ích ####
Một producer không cần phải biết consumer. Nó chỉ việc gửi message đến các queue trong message broker.
Consumer chỉ việc đăng ký nhận message từ các queue này
Vì producer giao tiếp với consumer trung gian qua message broker nên dù producer và consumer có khác biệt nhau về ngôn ngữ thì giao tiếp vẫn thành công.
Một đặc tính của rabbitmq là bất đồng bộ(asynchronous).
Producer không thể biết khi nào message đến được consumer hay khi nào message được consumer xử lý xong.
Đối với producer, đẩy message đến message broker là xong việc. Consumer sẽ lấy message về khi nó muốn. Đặc tính này có thể được tận dụng để xây dựng các hệ thống lưu trữ và xử lý log.

![](http://blog.ntechdevelopers.com/wp-content/uploads/2021/08/Image2.png)
Chính vì RabbitMQ là Message Queue nên nó cũng không ngoại lệ, nó nhắm giải quyết các bài toàn của Message Queue như bất đồng bộ, hệ thống phân tán, hay bài toán xử lý truyền dữ liệu hàng loạt.

Quay lại câu chuyện gửi thư, chả ai lại muốn phải đi hàng trăm cây số để gửi bức thư, chưa kể trong trường hợp phải gửi cho nhiều người ở các vùng khác nhau. Cực kỳ tốn kém chi phí. Vì vậy họ chọn các ký gửi cho bên thứ 3 (bưu điệm) với chi phí tốn kém ít hơn rất nhiều.

Bên cạnh nhưng ưu điểm của Message Queue thì RabbitMQ còn được cộng đồng dev review: mã nguồn mở(miễn phí), hiệu suất cao, hỗ trợ nhiều ngôn ngữ và giao thức khác nhau.
### Cơ chế hoạt động ###
Để hiểu RabbitMQ một cách đơn giản, ta có thể tưởng tượng nó như một cái bưu điện. Theo cách gọi của RabbitMQ, site A sẽ là Producer hay là người gửi thông điệp đi, còn các site khác chính là Consumer tức là người sẽ nhận các thông điệp được gửi từ site A. Khi đó, Producer sẽ kết nối tới Message broker thực hiện nhiệm vụ đẩy Message tới Consumer thông qua hệ thống Message broker.Một Message broker sẽ gồm 2 thành phần là exchange và queue. Trong đó, exchange có nhiều loại, việc chọn các loại exchange khác nhau sẽ tạo ra những cách đối xử khác nhau trên Message broker. Mỗi một exchange sẽ được liên kết với một queue nào đó. Nếu exchange là fanout thì massage sẽ được broadcast đi tới queue đã được kết nối với exchange.Các thành phần của RabbitMQ sẽ có nhiệm vụ như:
- Producer sẽ gửi tin đến máy chủ RabbitMQ.
- Exchange thực hiện phân phối tin theo các kiểu: Topic, Direct, Fanout, Headers.
- Queue được sử dụng để lưu trữ các bản tin đã được gửi tới.
- Consumer lấy bản tin về từ queue.
![](https://topdev.vn/blog/wp-content/uploads/2018/08/rabbitmq-beginners.png)
- Producer: Phía bên đảm nhận việc gửi message. Có thể xem đây là người cần gửi thư cho một ai đó.
- Consumer: Phía bên đảm nhận việc nhận message. Có thể xem đây là người nhận được thư mà ai đó gửi tới.
- Message: Thông tin dữ liệu truyền từ Producer đến Consumer. Đây chính là thư được gửi đi chứa nội dung gửi, nó có thể là thư tay, hàng hóa, bưu phẩm…
- Queue: Nơi lưu trữ messages. Có thể xem đây là một hòm lưu trữ thư với cơ chế, ai gửi trước thì được chuyển phát trước (First in first out)
- Connection: Kết nối giữa ứng dụng và RabbitMQ broker. Đây có thể coi là các bưu điện đặt tại các tỉnh thành, khi gửi thư thì sẽ phải ra bưu điện để gửi
- Channel: Một kết nối ảo trong một Connection. Quá trình gửi và nhận sẽ có một sợi dây liên kết trung gian, khi muốn gửi một bưu phẩm, ta sẽ phải ghi thông tin người gửi, người nhận lên trên một tờ phiếu gửi nhận, từ đó bưu điện sẽ dựa vào thông tin đó mà chuyển bưu phẩm.
- Exchange: Là nơi nhận message được publish từ Producer và đẩy chúng vào queue dựa vào quy tắc của từng loại Exchange. Có thể hiểu đây là một khu vực kho tổng hợp tất cả các thư mà mọi người gửi thư tới được tổng hợp, phân loại khu vực, gửi hàng loạt hay không…
- Binding: Đảm nhận nhiệm vụ liên kết giữa Exchange và Queue. Có thể xem đây là quá trình chuyển thừ hòm lưu trữ thư vào kho phân loại.
- Routing key: Một key mà Exchange dựa vào đó để quyết định cách để định tuyến message đến queue. Khi kiểm tra địa chỉ trên mỗi bức thư thì Routing key chính là địa chỉ người nhận, khi này việc phân loại thư trong kho sẽ phân loại dựa theo địa chỉ này để đưa tới từng khu vực bưu điện đích.
- AMQP: Giao thức Advance Message Queuing Protocol, là giao thức truyền message trong RabbitMQ. Có thể xem đây là một trong những hình thức chuyển phát bưu phẩm, có thể là chuyển phát nhanh, chuyển phát thường, chuyển phát đảm bảo…
- User: Gồm username và password giúp truy cập vào RabbitMQ dashboard hoặc tạo connection. Có thể xem đây là những nhân viên bưu điện, họ có thể theo dõi, phân loại, can thiệp, hỗ trợ trong quá trình gửi bưu phẩm.
- Virtual host/Vhost: Cung cấp những cách riêng biệt để các ứng dụng dùng chung một RabbitMQ instance. Hãy xem đây là những bưu cục chi nhánh rải trên khắp đất nước để thuận tiện cho người gửi cũng như người nhận.
### Các loại Exchange ###
#### Default Exchange ####

#### Direct Exchange ####

![](https://laptrinhvb.net/uploads/source/csharp/1_rabbit.png)
#### Fanout Exchange ####
![](https://laptrinhvb.net/uploads/source/csharp/2_rabbit.png)
#### Headers Exchange ####
![](https://laptrinhvb.net/uploads/source/csharp/4_rabbit.png)
#### Topic Exchange ####
![](https://laptrinhvb.net/uploads/source/csharp/3_rabbit.png)
#### Dead -Letter Exchange ####
queue: tên của Queue mà Message được publish trước khi nó trở thành Dead-Lettered Message.
reason: lý do xảy ra Dead-Lettered Message. Có thể là: rejected, expired, maxlen.
time: thời gian Message bị dead lettered.
exchange: tên Exchange mà Message được publish, nó có thể là dead letter exchange nếu Message bị dead lettered nhiều lần.
routing-keys: là routing key (bao gồm CC keys nhưng không bao gồm BCC keys) của Message được publish.
count: số lần mà Message bị dead-lettered trong Queue này vì lý do này.
original-expiration: nếu Message bị dead-lettered vì lý do TTL, nào là giá trị của thuộc tính expiration ban đầu của Message. Thuộc tính expiration sẽ bị remove từ dead-lettering message để ngăn việc expiring lần nữa trong Queue mà nó được định tuyến đến.
### Publisher Confirms ###
Có 2 callback:

Một cho Message được xác nhậ.
Một cho tin nhắn nack-ed (tin nhắn có thể được coi là bị mất bởi boker).
Mỗi callback có 2 tham số:

sequence number (số thứ tự): một số xác định Message được xác nhận hoặc nack-ed. Nó tương ứng với số Message được publish.
multiple: đây là một giá trị boolean. Nếu false, chỉ có một Message được xác nhận / nack-ed, nếu true, tất cả các Message có số thứ tự <= được xác nhận / nack-ed.
## Kafka ##
