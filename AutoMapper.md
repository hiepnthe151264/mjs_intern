# AutoMapper trong C# #
## Định nghĩa ##
Trong quá trình phát triển một ứng dụng web ASP.NET Core chúng ta thường cần chuyển đổi dữ liệu từ một object sang một object khác. Chúng ta có thể hoàn thành nhiệm vụ này thủ công bởi việc thiết lập những thuộc tính của object đích với những giá trị từ object nguồn. Nhưng sẽ là tốt hơn nếu công việc này có thể làm một cách tự động. Đó chính là cái mà thư viện AutoMapper có thể làm.  

AutoMapper trong C# là một object-object mapper. Nó map các thuộc tính giữa hai object khác nhau để chuyển đổi data của object này thành data của object khác.  

Ví dụ cho dễ hiểu này.  

Xem xét trang đăng kí người dùng được phát triển với ASP.NET Core MVC như hình bên dưới:
![](https://images.viblo.asia/bc5f4b75-32cf-4839-a75c-db6ca6ab91ac.png)  
Thông thường chúng ta sẽ lấy dữ liệu được nhập bởi người dùng trong trang này bằng cách sử dụng model binding. Và khi đó sẽ tạo một class view model:
```csharp
public class UserViewModel
{
    public string UserName { get; set; }

    public string Password { get; set; }

    public string ConfirmPassword { get; set; }

    public string Email { get; set; }

    public string FullName { get; set; }
}
```
Bây giờ, trong class UserViewModel đang có thuộc tính Password và ConfirmPassword là cần thiết bởi vì hệ thống validate cần chúng trong cách đó. Nhưng class Entity Framework Core không có thuộc tính ConfirmPassword được lưu trong bảng cơ sở dữ liệu. Tuy nhiên, class entity phải có thể thêm một thuộc tính khác như UserId, chúng không được nắm bắt bởi view model. Như vậy, class entity sẽ có định nghĩa như sau:  
```csharp
public class AppUser
{
    public int UserId { get; set; }

    public string UserName { get; set; }

    public string Password { get; set; }

    public string Email { get; set; }

    public string FullName { get; set; }
}
```
Bây giờ dữ liệu của bạn được ghi lại vào object UserViewModel và bạn cần làm thế nào đó chuyển nó vào object AppUser.  

Một cách tiếp cận đơn giản là bằng cách thủ công tạo object AppUser và thiết lập thuộc tính của nó với giá trị nhận được từ UserViewModel.  
```csharp
public IActionResult Register (UserViewModel model)
{
    if (ModelState.IsValid)
    {
        AppUser user = new AppUser()
        {
            UserName = model.UserName,
            Password = model.Password,
            Email = model.Email,
            FullName = model.FullName
        };
        db.Users.Add(user);
        db.SaveChanges();
    }
    return View();
}
```
Có thể dễ dàng nhận ra nếu trong trường hợp có nhiều Property thì code trên sẽ còn dài hơn nữa.  

Nếu sử dụng AutoMapper thì mọi thứ trở nên đơn giản hơn rất nhiều.  

Đầu tiên cần add thư viện AutoMapper trong Nuget Package, sau đó AddAutoMapper() vào phương thức ConfigureServices() và tạo 1 AutoMapperProfile.
```csharp
public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<UserViewModel, AppUser>();
    }
}
```
Class AutoMapperProfile thừa kế từ class base Profile. Constructor gọi phương thức CreateMap() nó chỉ định kiểu nguồn và đích để ánh xạ với nhau. Trong trường hợp này UserViewMapper là nguồn và AppUser là đích.  
```csharp
private readonly IMapper mapper;

public HomeController(IMapper mapper)
{
    this.mapper = mapper;
}

public IActionResult Register
(UserViewModel model)
{
    if (ModelState.IsValid)
    {
        AppUser user = mapper.Map<AppUser>(model);
        db.Users.Add(user);
        db.SaveChanges();
    }
    return View();
}
```
Code ở trên bao gồm 2 phần - Object IMapper được inject đến hàm constructor và Register().  

Object IMapper được inject bởi AutoMapper và được sử dụng cho việc ánh xạ các object như được minh họa trong hàm Register().  

Hàm Register sử dụng phương thức Map() của IMapper nó chỉ định kiểu đích và một object nguồn. Trong trường hợp này object UserViewModel nhận thông qua model binding, hành động như nguồn và tiếp theo nó được ánh xạ đến object AppUser.  

Trong trường hợp bạn cần ánh xạ các thuộc tính có điều kiện. Cho ví dụ, bạn có thể muốn ánh xạ DisplayName đến FullName chỉ nếu nó khác với UserName. Bạn có thể thực hiện giống như điều kiện ánh xạ bên dưới:
```csharp
public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<UserViewModel, AppUser>()
            .ForMember(destination => destination.FullName, 
options => options.MapFrom(source => source.DisplayName))
            .ForMember(destination => destination.FullName, 
options => options.Condition(source => source.DisplayName 
!= source.UserName));
    }
}
```
