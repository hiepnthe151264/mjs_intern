# 12 query optimization tips for better performance #
## Tips 1: Add missing indexes ##
## Tips 2: Checked for unused indexes ##
## Tips 3: Avoid using multiple OR clauses in the FILTER predicate ##
## Tips 4: Use wildcards at the end of a phrase only ##
## Tips 5: Avoid too many JOINs ##
## Tips 6: Avoid using SELECT DISTINCT ##
## Tips 7: SELECT specific columns instead of SELECT * ##
Nếu trong trường hợp ta chỉ cần vài cột của 1 bảng thì không nên dùng SELECT * , mặc dù cách này có vẻ dễ và ngắn gọn nhưng nó tốn bộ nhớ, tốn thời gian truy vấn, tốn dung lượng mạng hơn.
## Tips 8: Use TOP to sample query results ##
## Tips 9: Run queries during off-peak hours ##
## Tips 10: Minimize the usage of any query hints ##
## Tips 11: Minimize large write operations ##
## Tips 12:  ##
