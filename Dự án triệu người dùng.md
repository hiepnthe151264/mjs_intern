# Cách xây dựng Website triệu người dùng #
## Thởu còn sơ khai ##
Kiến trúc thiết kế ban đầu

![](https://images.viblo.asia/aebe1cbb-12c0-4cc4-b77b-eb3153f2220b.png)

Vấn đề: không đáp ứng được triệu người dùng, chết hệ thống.

Nguyên nhân: spam request bừa bãi

Giải quyết: Dùng rate limit để tránh một user spam request quá nhiều

Áp xong chạy được 1 thời gian ổn định thì lượng user tăng đột ngột gây ra vấn đề mới


Vấn đề 2: Người dùng tăng đột biến khiến máy chủ bị đơ, overloading,..
Nguyên nhân: Để các file tĩnh như html, css, ảnh,... ở phía server khiến cho mỗi lần user vào là load cả đống file tĩnh kia

Giải quyết: Có thể đẩy các file tĩnh sang một server nhưng vẫn sẽ tiềm ẩn rủi ro chết server hoặc có thể đẩy các file tĩnh này lên dịch vụ đám mây như BLOG storage như BLOG storage, AMZ S3,.... Và sau đố để ổn định hệ thống và tăng tính an toàn thì dùng dịch vụ CDN(content delivery network) như Cloudflare, Google cloud CDN,...

![](https://images.viblo.asia/7a955467-7c63-45d7-b5b4-065881c1d731.png)

Vấn đề 3: Sau khi đẩy các file tĩnh lên đám mây rồi, load file tĩnh ok rồi mà API vẫn treo

Nguyên nhân: hoặc là server hết tài nguyên hoặc là server không thể load kịp hết tất cả request 

Giải quyết: Đối với nguyên nhân server hết tài nguyên ta tiến hành nânng cấp theo chiều dọc (nâng RAM, CPU, ....) nếu vẫn không giải quyết được vấn đề thì ta nâng cấp theo chiều ngang (thêm các server chịu tải - cân bằng tải) để chia bớt request load cho các server khi user tăng lên.

Vấn đề 3 - phụ: nhưng mà request làm gì biết chạy vào server nào? domain trỏ vào mỗi một địa chỉ IP mà

Giải quyết: Dùng Reverse Proxy

Chú ý: phân biệt Forward Proxy và Reverse Proxy, Cái Forward Proxy như thằng trông cửa thỉnh thoảng mày nhờ nó chạy ra ngoài mua cái nọ cái kia, nghĩa là từ client muốn ra internet cần qua cái Forward Proxy. Còn cái Reverse Proxy như cái thằng điều khách, nó bảo khách A gặp em Mai, khách B gặp em Thúy ấy. Nghĩa là từ internet cần qua Reverse proxy để được chỉ định vào máy nào?

![](https://images.viblo.asia/362f677a-5d83-46a6-ac53-62816dff0d20.PNG)

Sau khi áp dụng Reverse Proxy ta được kiến trúc mới

![](https://images.viblo.asia/914b5a08-df7b-4c11-97a3-ee05082e9906.png)

Vấn đề 4: Bị nghẽn cổ chai database

Nguyên nhân: có quá nhiều request trỏ vào database

Giải quyết: 
- Dùng hệ thống cache. Thay vì vào database đọc dữ liệu, hệ thống sẽ đọc từ cache và từ đó giúp giảm thiểu request vào database, với lại đọc từ cache cũng nhanh hơn
    - Đọc 1MB từ RAM mất 0.25ms
    - Đọc 1M từ SSD mất 1ms
    - Đọc 1M từ mạng mất 10ms
    - Đọc 1M từ HDD mất 20ms
    - Đọc 1M từ Đông Lào qua Mỹ mất 150ms
- Có 4 loại cache là:
    - In-Memory cache
    - Distibution cache
    - Cache client
    - Tạng table cache trong DB
- Đầu tiên là với In-Memory cache, nó là việc cache dữ liệu ngay trên RAM của các API Server hoặc hash table ngay trong RAM để lúc cần đọc ra luôn. Nhưng nếu cache nhiều thì sẽ gây ra vấn đề tràn RAM => phải có một số giải pháp để xóa cache lâu không dùng, thông thường là dùng các thuật toán LRU (Least Recent Use) hoặc FIFO(First in First out) hoặc LFU(Least Frequently Use).

