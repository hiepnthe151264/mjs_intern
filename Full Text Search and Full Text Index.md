# FTS và FTI #
## Full Text Search ##
### Định nghĩa ###
Nói một cách đơn giản dễ hiểu thì FTS là một phương pháp để tìm kiếm thông tin nhất một cách tự nhiên nhất, hệt như google, ta chỉ cần gõ từ khoá và nhấn enter để nhận về kết quả.
FTS bao gồm một hoặc nhiều cột khác nhau dựa trên những ký tự có trong bảng. Các cột dữ liệu có thể thuộc các kiểu dữ liệu như: char, varchar, nchar, nvarchar, text, ntext, image, xml hoặc varbinary (max) và cả FILESTREAM.
### Lý do cần dùng FTS ###
Bình thường để truy vấn tìm kiếm theo tên, ta sẽ dùng
```sql
Select * from [Table_Name] Where Name Like '%keyword%'
```
Sử dụng Like sẽ có nhược điểm:
- Mặc dù đánh index để cải thiện tốc độ tìm kiếm nhưng khi đặt wildcard ở đầu hoặc giữa ('%keyword', 'key%word') thì sẽ không thể dùng index được mà nó sẽ duyệt theo kiểu Full Table Scan.
- Độ chính xác thấp và khả năng tìm kiếm bằng tiếng Việt rất tệ
- Độ nhiễu kết quả đầu ra cao
Nếu muốn có kết quả tìm kiếm bằng ngôn ngữ tự nhiên tốt thì ta nên sử dụng FTS
Yếu tố khiến FTS tốt hơn các phương pháp tìm kiếm khác đó là Inverted List
![](https://wiki.tino.org/wp-content/uploads/2021/09/word-image-637.png)
### Yếu tố khiến Inverted List nổi bật ###
Thông thường các phương phát tìm kiếm khác sẽ đánh index theo đơn vị row. Nhưng Inverted List thì lại khác, Inverted Index như một dạng cấu trúc dữ liệu có khả năng liên kết giữa các term với nhau và các tài liệu chứa term đó.
Ví dụ như ta có 3 tài liệu A1, A2, A3:
- A1: "This is first document"
- A2: "This is second one"
- A3: "one two"
Sau khi Inverted List 3 tài liệu trên ta được:
This => {A1, A2}
is => {A1, A2}
first => {A1}
document => {A1}
second => {A2}
one => {A2, A3}
two => {A3}
Giả sử ta muốn truy vấn cụm từ "This is first", FTS sẽ chuyển đổi bài toàn thành phép toán tử UNION
```sql
{A!, A2} UNION {A1, A2} UNION {A1} = {A1}
``` 
Vì vậy, khi bạn tìm kiếm dù cho các cụm từ đảo lộn thành: first This is hay This first is kết quả của phép toán union vẫn không đổi.
### Cách hoạt động của FTS ###
Khi các cột đã được lập chỉ mục, người dùng có thể sử dụng Full-Text Search theo các cách như sau:
- Một từ hoặc nhiều từ hoặc một cụm từ cụ thể (đơn giản nhất)
= Một hoặc một cụm từ bắt đầu bằng các văn bản được chỉ định (theo tiền tố)
= Một từ hoặc cụm từ gần giống với một từ hoặc một cụm từ khác (gần gũi)
= Các dạng đồng nghĩa của một cụm từ nào đó. Ví dụ trong tiếng Việt có đen, hắc, ô, mực.
= Các truy vấn (query) sẽ không phân biệt kết quả chữ in hoa hay in thường
## Full Text Index ##
