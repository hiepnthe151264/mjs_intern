<h1>Câu chuyện về Debug</h1>
<p>Chuyện là code lắm bug quá nên phải giảng hoà với bug bằng cách debug</p>
<h2>Các tính năng Debug</h2>
<ol>
    <li><b>Call Stack:</b> ...</li>
    <li><b>Local:</b> Cửa sổ Locals sẽ hiển thị tất cả các biến có liên quan đến dòng code hiện tại một cách tự động. Các biến hiển thị ở đây sẽ được thay đổi qua từng dòng code. Ngoài ra, còn có màu sắc để phân biệt được những biến nào vừa thay đổi giá trị.</li>
    <li><b>Autos:</b> Là con của local, cửa sổ Autos hiển thị các biến vừa được sử dụng trong các dòng code trước. Visual Studio sẽ tự động nhận diện biến nào không còn cần thiết và loại bỏ ra khỏi cửa sổ Autos.</li>
    <li><b>Watch:</b> Vì Visual Studio không thể nhận diện được tất cả những gì lập trình viên cần nên sinh ra watch cho thằng dev tự lực gánh sinh</li>
    <li><b>Data Tip:</b> chỉ vào biến thì nó hiện ra dữ liệu, nếu mà là kiểu object hoặc list thì phải kích thêm vào cái hình tam giác nhỏ để xem hết các giá trị</li>
    <li><b>Breakpoints(F9):</b> là vị trí mà chương trình sẽ dừng lại để dev có thể xem giá trị biến, trạng thái,....</li>
    <li><b>Breakpoints với Conditions:</b> vẫn là breakpoints mà phải thêm điều kiện thì mới dừng, ví dụ cho vòng lặp chạy từ i = 0 đến n mà cho điều kiện i == 5 thì dừng ở vòng lặp i = 5</li>
    <li><b>Immediate Window:</b> Thường thì mỗi lần thay đổi giá trị của biến là nhiều người sẽ debug lại từ đầu => mất thời gian build lại => immediate cho phép update giá trị của biến ngay tại thời điểm debug</li>
    <li><b>Edit and Continue:</b> Cũng giống như immediate nhưng bá hơn, sửa và chạy code ngay tại thời điểm debug nhưng chỉ có hiệu lực với các câu lệnh logic, còn thay đổi model thì build lại đi</li>
    <li><b>Threads Window:</b> Dùng trong multi-threading. Nó cho thông tin về các Threads đang chạy, điểm Breakpoint đang dừng đang được thực thi trên Thread nào (đánh dấu bằng mũi tên màu vàng)</li>
    <li><b>Data Breakpoints:</b> Trong các chương trình phức tạp, đôi khi chúng ta gặp trường hợp giá trị của một biến hay một vùng nhớ nào đó bị thay đổi nhưng có quá nhiều đoạn code truy cập đến thay đổi biến/vùng nhớ đó. Chúng ta sẽ rất khó khăn để debug theo cách thông thường vì méo biết đặt debug vào đâu => sinh ra Data Breakpoints với cơ chế làm cho trình debug dừng lại khi một vùng nhớ tại một địa chỉ cụ thể có sự thay đổi (muốn lấy địa chỉ biến thì mở immediate window xong gõ &Tenbien)</li>
    <li><b>Step Over(F10):</b> nôm na chỉ debug trong 1 file thôi, nếu có lấy kết quả ở hàm nào thì cũng chỉ nhận giá trị return của hàm</li>
    <li><b>Step Into(F11):</b> cũng như trên mà bay được thêm vào cả nội dung của hàm con</li>
    <li><b>Step Out(Shift + F11):</b> Nhảy sang breakpoints tiếp theo, nếu k còn thì end debug</li>
    <li>Ngoài ra còn vụ chạy dòng lệnh bằng mũi tên vàng ở bên trái với cơ chế on the fly nữa, ................</li>
</ol>
<h2>Exception là gì? Phân tích nó như thế nào cho tốt? Các loại Exception phổ biến</h2>
<h3>Tổng quan về exception</h3>
<p>Các exception có thể được tạo bởi thời gian chạy ngôn ngữ chung (CLR), .NET hoặc các thư viện của bên thứ ba hoặc bởi code của ứng dụng của bạn. Các ngoại lệ được tạo bằng cách sử dụng từ khóa throw. Trong nhiều trường hợp, một exception có thể được đưa ra không phải bởi một phương thức mà code của bạn đã gọi trực tiếp, mà bởi một phương thức khác nằm sâu hơn trong đó. Khi một exception được throw xảy ra, CLR sẽ giải phóng bộ nhớ, tìm kiếm một phương thức có khối catch cho loại exception cụ thể và nó sẽ thực thi khối catch đầu tiên nếu được tìm thấy. Nếu nó không tìm thấy khối bắt thích hợp ở bất kỳ đâu, nó sẽ kết thúc quá trình và hiển thị thông báo cho người dùng.</p>
<p>Exception có các thuộc tính sau</p>
<ul>
    <li>Có hai 2 loại exception trong .NET là exception tạo bởi chương trình thực thi và exception được tạo bởi CLR.</li>
    <li>Tất cả các exception đều được kế thừa trực tiếp hoặc gián tiếp từ lớp System.Exception</li>
    <li>Sử dụng khối try xung quanh các câu lệnh có thể tạo ra các exception.</li>
    <li>Khi một exception xảy ra trong khối try, luồng điều khiển sẽ chuyển đến trình xử lý ngoại lệ được liên kết đầu tiên có ở bất kỳ đâu trong ngăn xếp cuộc gọi. Trong C#, từ khóa catch được sử dụng để định nghĩa một trình xử lý ngoại lệ.</li>
    <li>Nếu không có trình xử lý exception nào cho một exception nhất định, chương trình sẽ ngừng thực thi với một thông báo lỗi.</li>
    <li>Đừng bắt một exception trừ khi bạn có thể xử lý nó và để ứng dụng ở trạng thái đã biết. Nếu bạn bắt System.Exception, hãy ném lại nó bằng cách sử dụng từ khóa throw ở cuối khối catch.</li>
    <li>Nếu một khối catch xác định một biến exception, bạn có thể sử dụng nó để lấy thêm thông tin về loại exception đã xảy ra.</li>
    <li>Các exception có thể được tạo một cách rõ ràng bởi một chương trình bằng cách sử dụng từ khóa throw.</li>
    <li>Các đối tượng exception chứa thông tin chi tiết về lỗi, chẳng hạn như trạng thái của ngăn xếp cuộc gọi và mô tả về lỗi.</li>
    <li>Mã trong khối finally được thực thi ngay cả khi một exception được ném ra. Sử dụng khối finally để giải phóng tài nguyên, chẳng hạn như để đóng bất kỳ luồng hoặc tệp nào đã được mở trong khối try.</li>
</ul>
<h3>Các loại exception phổ biến</h3>
<table>
    <tr>
        <th>Exception</th>
        <th>Miêu tả</th>
    </tr>
    <tr>
        <td>ArgumentException</td>
        <td>Xảy ra khi một đối số không null truyền vào một phương thức không hợp lệ</td>
    </tr>
    <tr>
        <td>ArgumentNullException</td>
        <td>Xảy ra khi một đối số null truyền vào một phương thức</td>
    </tr>
    <tr>
        <td>ArgumentOutOfRangeException</td>
        <td>Xảy ra khi giá trị của một đối số nằm ngoài phạm vi giá trị hợp lệ</td>
    </tr>
    <tr>
        <td>DivideByZeroException</td>
        <td>Xảy ra khi một số chia cho 0</td>
    </tr>
    <tr>
        <td>FileNotFoundException</td>
        <td>Xảy ra khi không tìm thấy file ở một vị trí chỉ định</td>
    </tr>
    <tr>
        <td>FormatException</td>
        <td>Xảy ra khi một giá trị không ở định dạng phù hợp được chuyển đổi từ chuỗi bằng phương thức chuyển đổi như Parse, Convert,...</td>
    </tr>
    <tr>
        <td>IndexOutOfRangeException</td>
        <td>Xảy ra khi một chỉ số mảng nằm ngoài giới hạn trên hoặc dưới của mảng hoặc Collection</td>
    </tr>
    <tr>
        <td>UnlimitedOperationException</td>
        <td>Xảy ra khi gọi một phương thức không hợp lệ trong trạng thái hiện tại của đối tượng</td>
    </tr>
    <tr>
        <td>UnlimitedCastException</td>
        <td>Xảy ra khi ép kiểu các dữ kiệu không phù hợp</td>
    </tr>
    <tr>
        <td>KeyNotFoundException</td>
        <td>Xảy ra khi khoá được chỉ định để truy cập phần tử trong Collection không tồn tại</td>
    </tr>
    <tr>
        <td>NotSupportedException</td>
        <td>Xảy ra khi một phương thước hoặc hoạt động không được hỗ trợ</td>
    </tr>
    <tr>
        <td>NullReferenceException</td>
        <td>Xảy ra khi chương trình truy cập vào đối tượng null</td>
    </tr>
    <tr>
        <td>OverFlowException</td>
        <td>Xảy ra khi hoạt động tính toán, ép kiểu hoặc chuyển đổi dẫn đến tràn bộ nhớ</td>
    </tr>
    <tr>
        <td>OutOfMemoryException</td>
        <td>Xảy ra khi một chương trình không có đủ bộ nhớ để thực thi mã</td>
    </tr>
    <tr>
        <td>SackOverFlowException</td>
        <td>Xảy ra khi một ngăn xếp trong bộ nhớ bị tràn</td>
    </tr>
    <tr>
        <td>TimeOutException</td>
        <td>Xảy ra khi khoảng thời gian được phân bổ cho một phương thức hoặc hoạt động đã hết hạn</td>
    </tr>
</table>
<h3>Các thuộc tính trong Exception</h3>
<table>
    <tr>
        <th>Thuộc tính</th>
        <th>Miêu tả</th>
    </tr>
    <tr>
        <td>Message</td>
        <td>Cung cấp thông tin chi tiết về nguyên nhân xảy ra Exception</td>
    </tr>
    <tr>
        <td>StackTrace</td>
        <td>Cung cấp thông tin về nơi xảy ra Exception</td>
    </tr>
    <tr>
        <td>InnerException</td>
        <td>Cung cấp thông tin về danh sách các Exception đã xảy ra</td>
    </tr>
    <tr>
        <td>HelpLink</td>
        <td>Thuộc tính này có thể lưu trữ URL trợ giúp cho một Exception cụ thể</td>
    </tr>
    <tr>
        <td>Data</td>
        <td>Thuộc tính này có thể lưu trữ dữ liệu tuỳ ý trong các cặp khoá - giá trị</td>
    </tr>
    <tr>
        <td>TargetSite</td>
        <td>Cung cấp tên của phương thức mà Exception này được đưa ra</td>
    </tr>
</table>
<h3>Những điểm cần nhớ</h3>
<ul>
    <li>
        System.Exception là lớp cơ sở cho bất kỳ kiểu ngoại lệ nào trong C#
    </li>
    <li>
        Có 2 kiểu ngoại lệ chính là System Exception(lỗi thời gian chạy lên quan đến CLR) và Application Exception
    </li>
    <li>
        Sử dụng các khối try, catch, finally để xử lý các Exception
    </li>
    <li>
        Có try thì phải có catch hoặc finally hoặc cả 2 theo sau
    </li>
    <li>
        Có thể dùng nhiều khối catch để bắt nhiều Exception khác nhau và catch(Exception e){} hoặc catch{} bắt buộc phải ở cuối cùng
    </li>
    <li>
        Không thể cùng tồn tài catch{} và catch(Exception e) trong 1 khối try catch
    </li>
    <li>
        Khối finally phải ở sau khối try hoặc catch
    </li>
    <li>
        Khối finally sẽ luôn được thi bất kể có Exception hay không
    </li>
    <li>
        Khối finally là nơi thích hợp để xử lý các đối tượng như đóng kết nối, xoá bộ nhớ đệm, huỷ đối tượng,.....
    </li>
    <li>
        Khối finally không thể sử dụng từ khoá return, continue, break vì nó không cho tự phép rời khỏi khối này
    </li>
    <li>
        Có thể dụng các khối try catch lồng nhau
    </li>
    <li>
        Một ngoại lệ sẽ được bắt và xử lý trong khối catch bên trong nếu tìm thấy bộ lọc thích hợp, nếu không nó sẽ bị bắt và xử lý bên ngoài khối catch
    </li>
    <li>
        Sử dụng throw thay vì throw ex để giữ stack trace giúp truy vết và xử lý lỗi dễ dàng
    </li>
</ul
<h3>Xử lý Exception trong C#</h3>
<p>C# cung cấp các khối try catch finally để xử lý exception. Khối try chỉ đơn giản là báo cho trình biên dịch biết cần phải theo dõi exception nếu nó xảy ra. Nếu ngoại lệ xảy ra trong khối try thì nó phải được xử lý bằng cách sử dụng khối catch. Code trong khối catch chỉ chạy khi có exception xảy ra. Trước khi khối catch được thực thi thì trình biên dịch sẽ kiểm tra các khối finally.</p>
<p>Nếu không tìm thấy khối catch tương thích nào trên ngăn xếp cuộc gọi sau khi một exception được xuất ra, một trong ba điều sẽ xảy ra:</p>
<ul>
    <li>Nếu exception nằm trong Finalize, Finalize sẽ bị hủy bỏ và nếu có base finalizer thì sẽ được gọi.</li>
    <li>Nếu ngăn xếp cuộc gọi chứa một phương thức khởi tạo static hoặc một trình khởi tạo static, thì một TypeInitializationException sẽ được xuất ra, với exception ban đầu được gán cho thuộc tính InnerException của exception mới.</li>
    <li>Nếu đạt đến điểm bắt đầu của thread, thì thread đó sẽ bị kết thúc</li>
</ul>
<p>Bất kỳ exception nào kế thừa từ lớp System.Exception đều có thể được ném ra bằng cách sử dụng từ khoá throw. Hoặc có thể dùng throw trong khối catch để ném cho trình xử lý exception phù hợp mà mình muốn. Nếu ném exception bằng tham số exception thì nó sẽ không giữ exception ban đầu mà sẽ tạo ra exception mới.</p>

<p>Khối finally thì nằm sau try hoặc catch. Code trong khối finally sẽ luôn được thực hiện dù có exception hay không. Thường dùng để đóng kết nối, xoá đối tượng,..</p>

```csharp
class Program
{
    static void Main(string[] args)
    {
        try
        {
            Method1();
        }
        catch(Exception ex)
        {
            Console.WriteLine(ex.StackTrace);
        }                      
    }

    static void Method1()
    {
        try
        {
            Method2();
        }
        catch(Exception ex)
        {
            throw ex;
        } 
    }

    static void Method2()
    {
        string str = null;
        try
        {
            Console.WriteLine(str[0]);
        }
        catch(Exception ex)
        {
            throw;
        }
    }
}
```
