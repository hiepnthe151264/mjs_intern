# ORM Framework (Dapper, Entity Framework, ADO.NET) #

## So sánh ##

<table>
    <tr>
        <th>Mô hình</th>
        <th>Ưu</th>
        <th>Nhược</th>
    </tr>
    <tr>
        <td rowspan="2">ADO.NET</td>
        <td>Thao tác bằng SQL Queries nên dễ dàng tunning để đạt performance cao, dễ dàng sửa đổi</td>
        <td>Code chậm (giải quyết bằng cách viết Store Procedure xong chỉ việc gọi đến để lấy dữ liệu)</td>
    </tr>
    <tr>
        <td>Mô hình ADO.NET cũng tương tác mạnh hơn với Procedure, đặc điểm phân trang mạnh, truy cập dữ liệu bất đồng bộ</td>
        <td></td>
    </tr>
    <tr>
        <td rowspan="7">Entity Framework</td>
        <td>Entity Framework cho phép bạn tạo mô hình bằng cách viết mã hoặc sử dụng các hộp và dòng trong EF Designer và tạo cơ sở dữ liệu mới</td>
        <td>Phải suy nghĩ theo cách xử lý dữ liệu phi truyền thống, không có sẵn cho mọi cơ sở dữ liệu</td>
    </tr>
    <tr>
        <td>Bạn có thể viết mã dựa trên Entity Framework và hệ thống sẽ tự động tạo ra các đối tượng cho bạn cũng như theo dõi các thay đổi trên các đối tượng đó và đơn giản hóa quá trình cập nhật cơ sở dữ liệu</td>
        <td>Nếu có bất kỳ thay đổi nào trong cơ sở dữ liệu thì lại phải cập nhập lại EF</td>
    </tr>
    <tr>
        <td>Một cú pháp chung (LINQ) cho tất cả các truy vấn đối tượng cho dù đó có phải là cơ sở dữ liệu hay không và khá nhanh nếu được sử dụng theo ý muốn, dễ thực hiện và ít cần mã hóa hơn để hoàn thành các tác vụ phức tạp</td>
        <td>Các truy vấn EF được tạo tự động nên không thể kiểm soát được</td>
    </tr>
    <tr>
        <td>EF có thể thay thế một lượng lớn mã mà bạn phải tự viết và duy trì</td>
        <td>Dùng cho dự án lớn, database to thì hiệu năng chậm</td>
    </tr>
    <tr>
        <td>Nó cung cấp mã được tạo tự động</td>
        <td></td>
    </tr>
    <tr>
        <td>Nó làm giảm thời gian và chi phí phát triển</td>
        <td></td>
    </tr>
    <tr>
        <td>Nó cho phép các nhà phát triển thiết kế trực quan các mô hình và ánh xạ cơ sở dữ liệu</td>
        <td></td>
    </tr>
    <tr>
        <td rowspan="5">Dapper</td>
        <td>Dễ dàng thực hiện các truy vấn (vô hướng, nhiều hàng, nhiều lưới và không có kết quả)</td>
        <td>Dapper không thể tạo mô hình lớp</td>
    </tr>
    <tr>
        <td>Dễ dàng chuyển kết quả thành đối tượng</td>
        <td>Không thể theo dõi các đối tượng và những thay đổi của chúng</td>
    </tr>
    <tr>
        <td>iệu quả và sở hữu danh hiệu Vua của Micro ORM về hiệu suất(nếu truy vấn ít thì ADO.NET hiệu suất vẫn sẽ cao hơn nhưng nếu truy suất liện tục + dữ liệu nhiều thì Dapper lại nhanh hơn)</td>
        <td>hư viện thô dapper không cung cấp các tính năng CRUD, nhưng gói bổ sung “đóng góp” cung cấp CRUD cơ bản</td>
    </tr>
    <tr>
        <td>Dapper giúp dễ dàng tham số hóa các truy vấn một cách chính xác</td>
        <td>Thời gian phát triển lâu hơn</td>
    </tr>
    <tr>
        <td></td>
        <td>Phải maintain các câu lệnh SQL trong source code (có thể kết hợp dùng thêm SQLKata code ngắn hơn mà cũng đỡ phải viết mấy câu truy vấn SQL)</td>
    </tr>
</table>

## So sánh hiệu năng ##
**Dùng 2000 bản ghi**
|      Lần     |   ADO.NET  |Regular Dapper|Entity Framework|
|:------------:|:----------:|:------------:|:--------------:|
|  Lần 1 (GET) |  45.4854ms |   60.0722ms  |    34.3201ms   |
|  Lần 2 (GET) |  55.0456ms |   14.6044ms  |    58.1219ms   |
|  Lần 3 (GET) |  62.7902ms |   19.6604ms  |    50.5406ms   |
|  Lần 4 (GET) | 151.0345ms |   9.485ms    |    47.0965ms   |
|  Lần 5 (GET) |  59.7957ms |   9.4941ms   |    16.9928ms   |
|   MIN (GET)  |  10.8208ms |   6.0028ms   |    16.9928ms   |
| Lần 1 (POST) |12864.6209ms| 12498.3541ms |   2119.3014ms  |
| Lần 2 (POST) |13122.6669ms| 12645.2334ms |   2045.4082ms  |
| Lần 3 (POST) |13363.7165ms| 12441.9326ms |   2145.3258ms  |
| Lần 4 (POST) |13382.2327ms| 12714.7363ms |   1691.6828ms  |
| Lần 5 (POST) |12856.6305ms| 12801.2434ms |   1856.5475ms  |
|   MIN (POST) |11891.0192ms| 12276.6093ms |   1592.4342ms  |
|  Lần 1 (PUT) |13796.261ms | 13863.4742ms |   4585.5449ms  |
|  Lần 2 (PUT) |13544.7766ms| 13782.5562ms |   1910.1227ms  |
|  Lần 3 (PUT) |13745.9548ms| 13336.0768ms |   1729.3652ms  |
|  Lần 4 (PUT) |14137.4327ms| 13778.0748ms |   1524.0282ms  |
|  Lần 5 (PUT) |13270.4568ms| 14094.9353ms |   1844.8377ms  |
|   MIN (PUT)  |13264.0938ms| 13062.7299ms |   1077.9961ms  |
|Lần 1 (DELETE)|  27.3372ms |   39.1506ms  |   1556.4508ms  |
|Lần 2 (DELETE)|  42.0707ms |   34.1308ms  |   1490.075ms   |
|Lần 3 (DELETE)|  31.5144ms |   39.8425ms  |   1483.4827ms  |
|Lần 4 (DELETE)|  49.7589ms |   28.9427ms  |   1031.1343ms  |
|Lần 5 (DELETE)|  34.8512ms |   27.6615ms  |   1126.4515ms  |
| MIN (DELETE) |  25.863ms  |   26.8913ms  |   1031.1343ms  |

**Dùng 100k bản ghi**
|      Lần     |    ADO.NET  |Regular Dapper|Entity Framework|Bulk Dapper (Bulk Insert, Update, Delete) + Contrib|BulkCopy ADO.NET|
|:------------:|:-----------:|:------------:|:--------------:|:-------------------------------------------------:|:--------------:|
|  Lần 1 (GET) |  343.9435ms |  235.0473ms  |   2164.4745ms  |                          NULL                     |                |
|  Lần 2 (GET) |  353.2717ms |  222.0506ms  |   657.4058ms   |                          NULL                     |                |
|  Lần 3 (GET) |  390.7299ms |  167.3047ms  |   546.6215ms   |                          NULL                     |                |
| Lần 1 (POST) |562602.8524ms|570092.1125ms |   30778.582ms  |                      8759.9985ms                  |                |
| Lần 2 (POST) |598225.0173ms|564652.0785ms |  29964.2428ms  |                      8304.983ms                   |                |
| Lần 3 (POST) |594804.3262ms|587390.3382ms |  28778.9811ms  |                      7940.1901ms                  |                |
|  Lần 1 (PUT) |  212.0213m  | 38625.7937ms |  27159.1809ms  |                      9982.2452ms                  |                |
|  Lần 2 (PUT) |66394.2761ms | 37823.0392ms |  23693.0141ms  |                      4285.7454ms                  |                |
|  Lần 3 (PUT) |62621.1261ms | 35216.6512ms |  22645.5462ms  |                      4122.5719ms                  |                |
|Lần 1 (DELETE)| 845.4587ms  |  1354.531ms  |  21314.4442ms  |                      7333.0527ms                  |                |
|Lần 2 (DELETE)| 1203.1548ms |  1332.9029ms |  21836.6649ms  |                      6913.2228ms                  |                |
|Lần 3 (DELETE)| 1081.263ms  |  887.0027ms  |  21584.5525ms  |                      7147.4468ms                  |                |

**PUBLIC**
|         Lần      |   ADO.NET  |BulkCopy ADO.NET|Regular Dapper|Bulk Dapper|EF Core 3.1|EF Core 6|
|:----------------:|:----------:|:--------------:|:------------:|:---------------------:|:-------:|:-------:|
|   1 ~ 10 (GET)   |            |                |              |                       |          |
|   1 ~ 30 (GET)   |            |                |              |                       |          |
|   1 ~ 50 (GET)   |            |                |              |                       |          |
|   1 ~ 10 (POST)  |            |                |              |                       |          |
|   1 ~ 30 (POST)  |            |                |              |                       |          |
|   1 ~ 50 (POST)  |            |                |              |                       |          |
|   1 ~ 10 (PUT)   |            |                |              |                       |          |
|   1 ~ 30 (PUT)   |            |                |              |                       |          |
|   1 ~ 50 (PUT)   |            |                |              |                       |          |
|   1 ~ 10 (DELETE)|            |                |              |                       |          |
|   1 ~ 30 (DELETE)|            |                |              |                       |          |
|   1 ~ 50 (DELETE)|            |                |              |                       |          |
|  10 ~ 10 (GET)   |            |                |              |                       |          |
| 10 ~ 30 (GET)    |            |                |              |                       |          |
| 10 ~ 50 (GET)    |            |                |              |                       |          |
| 10 ~ 10 (POST)   |            |                |              |                       |          |
| 10 ~ 30 (POST)   |            |                |              |                       |          |
| 10 ~ 50 (POST)   |            |                |              |                       |          |
| 10 ~ 10 (PUT)    |            |                |              |                       |          |
| 10 ~ 30 (PUT)    |            |                |              |                       |          |
| 10 ~ 50 (PUT)    |            |                |              |                       |          |
|10 ~ 10 (DELETE)  |            |                |              |                       |          |
|10 ~ 30 (DELETE)  |            |                |              |                       |          |
|10 ~ 50 (DELETE)  |            |                |              |                       |          |
| 100 ~ 10 (GET)   |            |                |              |                       |          |
| 100 ~ 30 (GET)   |            |                |              |                       |          |
| 100 ~ 50 (GET)   |            |                |              |                       |          |
| 100 ~ 10 (POST)  |            |                |              |                       |          |
| 100 ~ 30 (POST)  |            |                |              |                       |          |
| 100 ~ 50 (POST)  |            |                |              |                       |          |
| 100 ~ 10 (PUT)   |            |                |              |                       |          |
| 100 ~ 30 (PUT)   |            |                |              |                       |          |
| 100 ~ 50 (PUT)   |            |                |              |                       |          |
|100 ~ 10 (DELETE) |            |                |              |                       |          |
|100 ~ 30 (DELETE) |            |                |              |                       |          |
|100 ~ 50 (DELETE) |            |                |              |                       |          |
| 1000 ~ 10 (GET)  |            |                |              |                       |          |
| 1000 ~ 30 (GET)  |            |                |              |                       |          |
| 1000 ~ 50 (GET)  |            |                |              |                       |          |
| 1000 ~ 10 (POST) |            |                |              |                       |          |
| 1000 ~ 30 (POST) |            |                |              |                       |          |
| 1000 ~ 50 (POST) |            |                |              |                       |          |
| 1000 ~ 10 (PUT)  |            |                |              |                       |          |
| 1000 ~ 30 (PUT)  |            |                |              |                       |          |
| 1000 ~ 50 (PUT)  |            |                |              |                       |          |
|1000 ~ 10 (DELETE)|            |                |              |                       |          |
|1000 ~ 30 (DELETE)|            |                |              |                       |          |
|1000 ~ 50 (DELETE)|            |                |              |                       |          |
| 10000 ~ 10 (GET) |            |                |              |                       |          |
| 10000 ~ 30 (GET) |            |                |              |                       |          |
| 10000 ~ 50 (GET) |            |                |              |                       |          |
| 10000 ~ 10 (POST)|            |                |              |                       |          |
| 10000 ~ 30 (POST)|            |                |              |                       |          |
| 10000 ~ 50 (POST)|            |                |              |                       |          |
| 10000 ~ 10 (PUT) |            |                |              |                       |          |
| 10000 ~ 30 (PUT) |            |                |              |                       |          |
| 10000 ~ 50 (PUT) |            |                |              |                       |          |
|10000 ~ 10 (DELETE)|            |                |              |                       |          |
|10000 ~ 30 (DELETE)|            |                |              |                       |          |
|10000 ~ 50 (DELETE)|            |                |              |                       |          |
| 100000 ~ 10 (GET) |            |                |              |                       |          |
| 100000 ~ 30 (GET) |            |                |              |                       |          |
| 100000 ~ 50 (GET)     |            |                |              |                       |          |
| 100000 ~ 10 (POST)    |            |                |              |                       |          |
| 100000 ~ 30 (POST)    |            |                |              |                       |          |
| 100000 ~ 50 (POST)    |            |                |              |                       |          |
| 100000 ~ 10 (PUT)     |            |                |              |                       |          |
| 100000 ~ 30 (PUT)     |            |                |              |                       |          |
| 100000 ~ 50 (PUT)     |            |                |              |                       |          |
|100000 ~ 10 (DELETE)   |            |                |              |                       |          |
|100000 ~ 30 (DELETE)   |            |                |              |                       |          |
|100000 ~ 50 (DELETE)   |            |                |              |                       |          |

## Phân tích các trường hợp sử dụng ##
