#SƠ RI SQL - EXECUTION PLAN#
## ĐỊNH NGHĨA ##
**Execution Plan** là bản kế hoạch chi tiết những gì phải làm khi thực thi một câu lệnh sql. **Query optimizer** sẽ là thằng phụ trách việc chọn cách truy vấn như thế nào cho phù hợp với các tham số đầu vào. **Query optimizer** sẽ sử dụng các thông tin cần thiết như index, statitics, construct,... để tìm ra **Execution Plan** được cho là là **tốt**, chứ không phải **tốt nhất**. Kết quả của quá trình trên gọi là **Compiled Plan** và SQL sẽ theo từng bước trong plan này để đạt được kết quả. Tuy nhiên quá trình tạo ra Compiled Plan rất tốn thời gian và tài nguyên nên vì vậy việc giảm chi phí tạo Execution Plan rất quan trọng, góp phần giảm tải công việc cho Sql Server. Thực tế, thời gian thực thi câu truy vấn = thời gian tạo Execution Plan + thời gian thực thi câu truy vấn dựa trên Execution Plan đó. Do đó, **Compiled Plan** sẽ được lưu lại vào **Plan Cache** hay còn gọi là **Procedure Cache** để dùng tiếp và giảm thiểu bước tạo Execution Plan.
![](https://quantricsdulieu.com/wp-content/uploads/2021/01/flow-1.png)
## DẠNG XEM CỦA EXECUTION PLAN ##
Execution Plan có 3 dạng xem là text, xml và graphic (trên thanh công cụ)
```sql
USE 'TEN_DATABASE'
GO
 
-- execution plan dạng text
SET SHOWPLAN_TEXT ON
GO
--- điều kiện tìm kiếm ở mệnh đề HAVING
SELECT 'CONDITION'
FROM 'TEN_TABLE' u
    INNER JOIN 'TEN_TABLE' p 'CONDITION'
WHERE 'CONDITION'
GROUP BY 'CONDITION'
HAVING 'CONDITION'
ORDER BY 'CONDITION'
OPTION ('CONDITION')
GO
SET SHOWPLAN_TEXT OFF
 
GO
--- execution plan dạng XML
SET SHOWPLAN_XML ON
GO
--- điều kiện tìm kiếm ở mệnh đề HAVING
SELECT 'CONDITION'
FROM 'TEN_TABLE' u
    INNER JOIN 'TEN_TABLE' p ON 'CONDITION'
WHERE 'CONDITION'
GROUP BY 'CONDITION'
HAVING 'CONDITION'
ORDER BY 'CONDITION'
OPTION ('CONDITION')
GO
SET SHOWPLAN_XML OFF
```
## CÁC DẠNG EXECUTION PLAN ##
Ngay khi chạy câu lệnh SQL một **Execution Plan** được sinh ra dưới dạng text và xml gọi là **Estimated Plan(ước tính)**. Phải đợi đến sau khi câu lệnh SQL trả về tất cả row thì mới sinh ra **Actual Plan(thực tế)** ở dạng graphic với đầy đủ các thông số như row đã đọc, cost,....
## PLAN CACHE ##
Như đã đề cập ở trên dùng xong rồi thì phải cất đi có việc còn có sẵn mang ra dùng, vì chi phí tạo ra **Execution Plan** rất tốn kém nên không thể dùng xong là vứt đi luôn được nên **Execution Plan** sẽ lưu vào **Plan Cache** ở dạng **Estimated Plan**.

Tại sao lại là **Estimated Plan** chứ không phải **Actual Plan**? Đơn giản là **Estimated Plan** sẽ gọn hơn mà vẫn có đầy đủ kế hoạch thực hiện còn **Actual Plan** thì hơi lắm chỉ số thừa không cần dùng đến (như kiểu ở máy khác, bối cảnh khác thì thông số lại khác) lưu vào **Cache** tốn dung lượng hơn.

Mỗi **Compiled Plan** sẽ có một giá trị **plan_handle** và duy nhất trong **Plan Cache**, có thể xem như là ID của **Compiled Plan**. Dùng giá trị **plan_handle** này để lấy ra XML plan từ **DMF sys.dm_exec_query_plan**. Tuy nhiên, với sự hỗ trợ của SSMS 18.5 bạn chỉ cần click vào nó sẽ convert sang **Graphical Execution Plan** để bạn dễ nhìn hơn, hoặc bạn có thể tự lưu nội dung XML này thành file có đuôi .sqlplan rồi mở bằng SSMS cũng sẽ nhận được **Graphical Execution Plan**.
## EXECUTION PLAN ĐƯỢC LƯU TRONG PLAN CACHE NHƯ THẾ NÀO? ##
Khi một **Execution Plan** mới được lưu vào **Plan Cache**, tất nhiên kích cỡ của **Plan Cache** sẽ phát triển, hay nói cách khác, lượng bộ nhớ để lưu trữ **Execution Plan** sẽ tăng lên. Bộ nhớ thì lại có giới hạn, không thể lưu hết tất cả **Execution Plan** được tạo ra, **SQL Server** sẽ tự động điều chỉnh số lượng **Execution Plan** trong **Plan Cache** bằng cách giữ lại những **Execution Plan** thường được sử dụng, loại bỏ những **Execution Plan** không được sử dụng sau một khoảng thời gian nhất định. 

Ừm thế thì **SQL Server** nhận biết các **Execution Plan** thường được sử dụng như thế nào? Câu trả lời là **SQL Server** sẽ gán tuổi cho các **Execution Plan** được tạo ra, tuổi của **Execution Plan** chính là chi phí để tạo ra **Execution Plan** đó. Một câu truy vấn phức tạp sẽ có tuổi cao hơn một câu truy vấn đơn giản, vì chi phí tạo ra **Execution Plan** của câu truy vấn phức tạp cao hơn câu truy vấn đơn giản.
Sau một khoảng thời gian xác định, tuổi của các **Execution Plan** sẽ bị giảm bởi tiến trình **Lazy Writer**. Tiến trình **Lazy Writer** sẽ chịu trách nhiệm quản lý các tiến trình chạy ngầm trong **SQL Server**. 

Nếu một **Execution Plan** không được sử dụng trong một thời gian dài, tuổi của nó sẽ giảm xuống còn 0. Chi phí tạo ra **Execution Plan** càng rẻ, tuổi của nó sẻ càng nhanh chóng giảm xuống còn 0, và sẽ bị loại bỏ khỏi bộ nhớ nếu **SQL Server** cần bộ nhớ cho những việc khác. Tuy nhiên, nếu hệ thống còn dư bộ nhớ, những **Execution Plan** có tuổi là 0 vẫn sẽ được giữ lại một thời gian dài để sử dụng lại nếu cần.
Mỗi khi **Execution Plan** được tái sử dụng, tuổi của nó sẽ tăng lên bằng cách cộng thêm chi phí tạo ra **Execution Plan** đó. Ví dụ có một **Execution Plan** được tạo ra với chi phí là 100, nó sẽ có tuổi là 100. Khi **Execution Plan** này được tái sử dụng, tuổi của nó sẽ tăng lên là 200. Nếu không được tái sử dụng thường xuyên, tuổi của **Execution Plan** sẽ bị giảm dần bởi tiến trình **Lazy Writer** như đã nói ở trên.

_Lazy Writer: Cái này chuyển sâu với khó, DBA cũng không kiểm soát được, thấy bảo nếu Lazy Writer mà chạy liên tục thì gây ra Bottleneck memory ( dọn rác mà lại làm tắc nghẽn bộ nhớ :)) )._
## BÀN SÂU HƠN VỀ CÂU CHUYỆN DÙNG LẠI EXECUTION PLAN ##
Khi một câu truy vấn SQL được submit, **Query Optimizer** tất nhiên sẽ tạo ra một **Execution Plan** và lưu nó vào **Plan Cache** để dùng lại lần sau. Nếu câu truy vấn này được submit với một giá trị khác, nó có thể dùng lại **Execution Plan** được tạo ra trước đó. Nhưng **Execution Plan** có được dùng lại hay không phụ thuộc vào cách câu truy vấn được gửi đến **SQL Server**.

||Viết đúng cách|Viết không đúng cách|
|:-:|:-:|:-:|
|Số lần thực thi câu truy vấn|5|5|
|Số Execution Plan tạo ra|1|5|
|Số lần bắt CPU làm việc để tạo ra Execution Plan|1|5|
|Số Execution Plan được lưu trong bộ nhớ|1|5|

Khi một câu truy vấn được gửi đến **SQL Server**, nó được phân loại ra làm 2 kiểu sau:
- **Ad Hoc:**
    - Những câu truy vấn mà không tách biệt rõ ràng các biến thành tham số thì gọi là **Ad Hoc Workloads** hay còn gọi là **Ad Hoc Queries**. Ví dụ như : SELECT * FROM TABLE WHERE ID = 1;
    - Trong câu truy vấn trên, điều kiện là ID = 1 được nhúng trực tiếp vào câu truy vấn mà không tách biệt ra thành tham số, điều này làm cho Execution Plan tạo ra bởi câu truy vấn này không thể tái sử dụng, trừ khi bạn sử dụng lại cùng giá trị, ở đây là ID = 1. Cái này trước có đọc rồi mà nó tiện nên thành ra chả quan tâm bởi dữ liệu nhỏ nên nó chả ảnh hưởng lắm. Trừ khi kiểu bị phân mảnh nặng thì mới chết.
- **Prepared:**
    - Những câu truy vấn được gửi đến **SQL Server** mà tách biệt rõ ràng các biến thành những tham số, không nhúng giá trị biến trực tiếp vào câu truy vấn, gọi là **Prepared Workload**. Có 3 kiểu viết để câu truy vấn được phân loại là **Prepared Workload:**
        - **Stored Procedures:** Kiểu này đã quá quen thuộc với người dùng **SQL Server** từ cơ bản đến nâng cao.
        - **Sp_executesql:** cho phép thực thi câu lệnh SQL hay một chuỗi các câu lệnh SQL (gọi là **SQL Batch**) có chứa những tham số do người dùng định nghĩa (trông hơi lạ).
        - **Prepare/execute model:** cho phép một **SQL client** yêu cầu một Execution Plan cho những lần thực thi sau này của một câu lệnh SQL giống nhau, khác tham số (chưa thấy bao giờ).
    - Nếu muốn tối ưu performance thì tất nhất cứ nên dùng Prepared để tối ưu Execution Plan.
## ĐỌC HIỂU EXECUTION PLAN NHƯ THẾ NÀO? ##
Chịu, trông nó giống kiểu trình tự thực hiện logic trong 1 câu lệnh SQL (các từ khoá) nhưng mà bị lược bỏ mất vài từ khoá như FROM, ON,....
